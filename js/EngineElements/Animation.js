/**
 * This class represents an animation.
 */

function Animation(img, refreshRate, size){
    // PUBLIC ATTRIBUTES -------------------------------------------------------------------------//
    
    // PRIVATE ATTRIBUTES ------------------------------------------------------------------------//
    var that = this;
    var log = new Log("ANIMATION");

    var image;
    var timePerFrame;
    var frameSize;

    var timeInFrame = 0;
    var currentFrame = 0;
    var frames;

    // PUBLIC METHODS ----------------------------------------------------------------------------//

    // EVENT METHODS -----------------------------------------------------------------------------//
    this.initialize = function() {}

    this.update = function(deltaTime) {
        timeInFrame += deltaTime;
        if(timeInFrame > timePerFrame){
            currentFrame = ++currentFrame >= frames ? 0 : currentFrame;
            timeInFrame -= timePerFrame;
        }
    }

    this.render = function(renderer, position, size) {
        renderer.save();

        var clipX = currentFrame * frameSize.x;
        var clipY = 0;
        var clipW = frameSize.x;
        var clipH = frameSize.y;
        var width = size.x;
        var height = size.y;
        var x = position.x;
        var y = position.y;
        
        renderer.drawImage(img, clipX, clipY, clipW, clipH, x, y, width, height);

        renderer.restore();
    }

    // CONSTRUCTOR -------------------------------------------------------------------------------//
    image = img;
    timePerFrame = 1/refreshRate;
    frameSize = size;
    frames = img.width/frameSize.x;

    // INTERNAL METHODS --------------------------------------------------------------------------//

}
