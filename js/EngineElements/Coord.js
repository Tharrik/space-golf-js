function Coord(x, y, z) {

    this.x = x != undefined ? x : 0;
    this.y = y != undefined ? y : 0;
    this.z = z != undefined ? z : 0;
}

// Returns a copy of the Coord
Coord.prototype.copy = function() { return new Coord(this.x, this.y, this.z); }

// Sets the values of the Coord
Coord.prototype.set = function(x, y, z) {
    this.x = x != undefined ? x : 0;
    this.y = y != undefined ? y : 0;
    this.z = z != undefined ? z : 0;
}

// Returns a new Coord equal to this Coord plus the other Coord
Coord.prototype.add = function(other) {
    var coord = this.copy();
    if(other == undefined) return coord;
    if(other instanceof Coord){
      coord.x += (other.x != undefined ? other.x : 0);
      coord.y += (other.y != undefined ? other.y : 0);
      coord.z += (other.z != undefined ? other.z : 0);
    }
    return coord;
}
// Returns a new Coord equal to this Coord minus the other Coord
Coord.prototype.subtract = function(other){
    var coord = this.copy();
    if(other == undefined) return coord;
    if(other instanceof Coord){
      coord.x -= (other.x != undefined ? other.x : 0);
      coord.y -= (other.y != undefined ? other.y : 0);
      coord.z -= (other.z != undefined ? other.z : 0);
    }
    return coord;
}

Coord.prototype.multiply = function(mult) {
    var coord = this.copy();
    if(typeof(mult) == "number"){
        coord.x = this.x * mult;
        coord.y = this.y * mult;
        coord.z = this.z * mult;
    }
    return coord;
}

Coord.prototype.module = function() {
    return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
}

Coord.prototype.normalize = function() {
    return this.multiply(1/this.module());
}

Coord.prototype.toString = function() {
    return "(" + this.x + ", " + this.y + ", " + this.z + ")";
}

Coord.prototype.random = function(bounds) {
    if(!bounds || !(bounds instanceof Rectangle)) { return; }
    var x = bounds.position.x + Math.random() * bounds.size.x;
    var y = bounds.position.y + Math.random() * bounds.size.y;
    return new Coord(x, y);
}
