/**
 * This class represents a rigidbody.
 */

function Hawking(){
    // STATIC MEMBERS ----------------------------------------------------------------------------//

    // PUBLIC ATTRIBUTES -------------------------------------------------------------------------//
    
    // PRIVATE ATTRIBUTES ------------------------------------------------------------------------//
    var that = this;

    var gravity = new Coord(0, 9.8);

    // PUBLIC METHODS ----------------------------------------------------------------------------//

    // EVENT METHODS -----------------------------------------------------------------------------//
    this.initialize = function() {}
    this.update = function(deltaTime) {}
    this.render = function(renderer) {}

    // CONSTRUCTOR -------------------------------------------------------------------------------//
    
    // INTERNAL METHODS --------(Use that instead of this)----------------------------------------//

}
