function InputManager(canvas){
    // TODO implement context usage
    this.canvas = canvas;

    this.contextEnum = Object.freeze({MENU:1, GAME:2});
    this.context = this.contextEnum.GAME;

    // Initialize game map
    InputManager.prototype.gameMap.set(InputManager.prototype.states.DOWN, false);
    InputManager.prototype.gameMap.set(InputManager.prototype.states.UP, false);
    InputManager.prototype.gameMap.set(InputManager.prototype.states.RIGHT, false);
    InputManager.prototype.gameMap.set(InputManager.prototype.states.LEFT, false);
    InputManager.prototype.gameMap.set(InputManager.prototype.states.FIRE, false);
    InputManager.prototype.gameMap.set(InputManager.prototype.actions.PAUSE, false);

    // Events
    var selfReference = this;
    addEventListener("mousemove", function(evt){
        selfReference.OnMouseMove(evt);
    });

    addEventListener("keydown", function(evt){
        selfReference.OnKeyDown(evt);
    });

    addEventListener("keyup", function(evt){
        selfReference.OnKeyUp(evt);
    });

    addEventListener("mousedown", function(evt){
        selfReference.OnMouseDown(evt);
    });

    addEventListener("mouseup", function(evt){
        selfReference.OnMouseUp(evt);
    });
}

// Singleton
InputManager.prototype.instance;

InputManager.prototype.getInstance = function(){
    if(InputManager.prototype.instance == undefined){
        InputManager.prototype.instance = new InputManager();
    }
    return InputManager.prototype.instance;
}

// Set the canvas
InputManager.prototype.setCanvas = function(canvas){
    this.canvas = canvas;
}

// Keyboard game map
InputManager.prototype.gameMap = new Map();

InputManager.prototype.mousePosition = new Coord();

// Keyboard mapping
InputManager.prototype.keyCodeEnum = {
    W: 87,
    A: 65,
    S: 83,
    D: 68,
    UP: 38,
    DOWN: 40,
    LEFT: 37,
    RIGHT: 39,
    SPACEBAR: 32,
    PAUSE: 19,
    P: 80
};

InputManager.prototype.actions = Object.freeze({
    PAUSE: "pause"
})

InputManager.prototype.states = Object.freeze({
    RIGHT: "right",
    LEFT: "left",
    DOWN: "down",
    UP: "up",
    FIRE: "fire",
});

InputManager.prototype.setContext = function(ctx){
    this.context = ctx;
}

InputManager.prototype.getContextMap = function(){
    switch (this.context) {
        case this.contextEnum.GAME:
            return this.gameMap;
            break;
        default:
    }
}

InputManager.prototype.OnMouseMove = function(evt){
    var canvasRect = this.instance.canvas.getBoundingClientRect();
    this.mousePosition.x = evt.clientX - canvasRect.left;
    this.mousePosition.y = evt.clientY - canvasRect.top;
    // console.log("Mouse moved to ( " + this.mousePosition.x + ", " + this.mousePosition.y + " )");
}

InputManager.prototype.OnKeyDown = function(evt){
    var key = evt.which | evt.keyCode;
    // console.log("Keydown event called. KeyCode: " + key);
    switch (key) {
        case InputManager.prototype.keyCodeEnum.W:
        case InputManager.prototype.keyCodeEnum.UP:
        case InputManager.prototype.keyCodeEnum.SPACEBAR:
            InputManager.prototype.gameMap.set(InputManager.prototype.states.UP, true);
            break;
        case InputManager.prototype.keyCodeEnum.S:
        case InputManager.prototype.keyCodeEnum.DOWN:
            InputManager.prototype.gameMap.set(InputManager.prototype.states.DOWN, true);
            break;
        case InputManager.prototype.keyCodeEnum.A:
        case InputManager.prototype.keyCodeEnum.LEFT:
            InputManager.prototype.gameMap.set(InputManager.prototype.states.LEFT, true);
            break;
        case InputManager.prototype.keyCodeEnum.D:
        case InputManager.prototype.keyCodeEnum.RIGHT:
            InputManager.prototype.gameMap.set(InputManager.prototype.states.RIGHT, true);
            break;
        case InputManager.prototype.keyCodeEnum.PAUSE:
            InputManager.prototype.gameMap.set(InputManager.prototype.actions.PAUSE, true);
            break;
    }
}

InputManager.prototype.OnKeyUp = function(evt){
    var key = evt.which | evt.keyCode;
    // console.log("Keyup event called. KeyCode: " + key);
    switch (key) {
        case InputManager.prototype.keyCodeEnum.W:
        case InputManager.prototype.keyCodeEnum.UP:
        case InputManager.prototype.keyCodeEnum.SPACEBAR:
            InputManager.prototype.gameMap.set(InputManager.prototype.states.UP, false);
            break;
        case InputManager.prototype.keyCodeEnum.S:
        case InputManager.prototype.keyCodeEnum.DOWN:
            InputManager.prototype.gameMap.set(InputManager.prototype.states.DOWN, false);
            break;
        case InputManager.prototype.keyCodeEnum.A:
        case InputManager.prototype.keyCodeEnum.LEFT:
            InputManager.prototype.gameMap.set(InputManager.prototype.states.LEFT, false);
            break;
        case InputManager.prototype.keyCodeEnum.D:
        case InputManager.prototype.keyCodeEnum.RIGHT:
            InputManager.prototype.gameMap.set(InputManager.prototype.states.RIGHT, false);
            break;
        case InputManager.prototype.keyCodeEnum.PAUSE:
            InputManager.prototype.gameMap.set(InputManager.prototype.actions.PAUSE, false);
            break;
        }
    }

InputManager.prototype.OnMouseDown = function(evt){
    var button = evt.button;
    switch (button) {
        case 0:
            InputManager.prototype.gameMap.set(InputManager.prototype.states.FIRE, true);
            break;
    }
}

InputManager.prototype.OnMouseUp = function(evt){
    var button = evt.button;
    switch (button) {
        case 0:
            InputManager.prototype.gameMap.set(InputManager.prototype.states.FIRE, false);
        break;
    }
}
