/**
 * This class represents ...
 */

function Rectangle(position, size){
    // STATIC MEMBERS ----------------------------------------------------------------------------//

    // PUBLIC ATTRIBUTES -------------------------------------------------------------------------//
    this.position;
    this.size;      // Can't have negative values yet!

    // PRIVATE ATTRIBUTES ------------------------------------------------------------------------//

    // PUBLIC METHODS ----------------------------------------------------------------------------//
    this.top = function() { return this.position.x; }
    this.bottom = function() { return this.position.x + this.size.x; }
    this.left = function() { return this.position.y; }
    this.right = function() { return this.position.y + this.size.y; }

    this.copy = function() { return new Rectangle(this.position.copy(), this.size.copy()); }

    /** Returns true if the given point (Coord) is inside this rectangle. */
    this.contains = function(coord){
        if(!coord || !(coord instanceof Coord)) { return; }
        if(coord.x >= this.top() && coord.y <= this.bottom()
        && coord.y >= this.left() && coord.y <= this.right()) {
            return true;
        } else {
            return false;
        }
    }

    // EVENT METHODS -----------------------------------------------------------------------------//

    // CONSTRUCTOR -------------------------------------------------------------------------------//
    this.position = position ? position: new Coord(0,0);
    this.size = size ? size: new Coord(10, 10);
    
    // INTERNAL METHODS --------------------------------------------------------------------------//

}
