/**
 * This class represents a renderer used to control drawing and size aspects in a game.
 * @param {Coord} size The virtual size of the window.
 */

function Renderer(size){
    // PUBLIC ATTRIBUTES -------------------------------------------------------------------------//
    this.debug = true;
    
    this.size;
    this.fillStyle;
    this.strokeStyle;
    this.lineWidth;
    
    // PRIVATE ATTRIBUTES ------------------------------------------------------------------------//
    var canvas;
    var context;
    var scale;
    var resized = false;

    var log = new Log("RENDERER");

    var that = this;

    // PUBLIC METHODS ----------------------------------------------------------------------------//
    /** Resizes the canvas used to draw to fit the virtual aspect ratio. It always makes the canvas
     * smaller so it does not resize out of the html intended size.
     * @param {Coord} size The virtual size of the window.
     */
    this.resizeCanvas = function(size) {
        if(resized && size && size.x == this.size.x && size.y == this.size.y){
            log.d("No need to resize.");
            return;
        }
        this.size = size == undefined ? this.size : size;
        log.d("Resizing to {" + this.size.x + ", " + this.size.y + "}");
        var initial_ratio = canvas.width/canvas.height;
        var wanted_ratio = this.size.x/this.size.y;

        if(initial_ratio < wanted_ratio) {
            canvas.height = canvas.width / wanted_ratio;
        } else if(initial_ratio > wanted_ratio){
            canvas.width = canvas.height * wanted_ratio;
        }

        scale = canvas.width / this.size.x;
        resized = true;
    }

    this.center = function() { return this.size.multiply(0.5); }

    this.createLinearGradient = function(x0, y0, x1, y1){
        return context.createLinearGradient(x0 * scale, y0 * scale, x1 * scale, y1 * scale);
    }

    /** Saves the current state of the context. */
    this.save = function() {
        context.save();
    }

    /** Restores the saved state of the context. */
    this.restore = function() {
        context.restore();
        this.fillStyle = context.fillStyle;
        this.strokeStyle = context.strokeStyle;
        this.lineWidth = context.lineWidth;
    }

    /** Draws the current path. */
    this.stroke = function() {
        updateStyles()
        context.stroke();
    }

    /** Fills the current path. */
    this.fill = function() {
        updateStyles()
        context.fill();
    }

    /** Creates a rectangle path that will be drawn by calling stroke().
     * @param {*} x Position X
     * @param {*} y Position Y
     * @param {*} w Width
     * @param {*} h Height
     */
    this.rect = function(x, y, w, h) {
        context.rect(x * scale, y * scale, w * scale, h * scale);
    }

    /** Begins a new path, erasing the previous one. */
    this.beginPath = function(){
        context.beginPath();
    }

    /** Moves the path to the specified point, without creating a line. 
     * @param {*} x 
     * @param {*} y 
     */
    this.moveTo = function(x, y) {
        context.moveTo(x * scale, y * scale);
    }

    /** Adds a new point to the current path, connecting it with the previous one.
     * @param {*} x 
     * @param {*} y 
     */
    this.lineTo = function(x, y) {
        context.lineTo(x * scale, y * scale);
    }

    /** Closes the current path connecting the first and last points. */
    this.closePath = function() {
        context.closePath();
    }

    /** Draws a rectangle area.
     * @param {*} x Position X
     * @param {*} y Position Y
     * @param {*} w Width
     * @param {*} h Height
     */
    this.fillRect = function(x, y, w, h) {
        updateStyles();
        context.fillRect(x * scale, y * scale, w * scale, h * scale);
    }

    /** Draws a rectangle perimeter.
     * @param {*} x Position X
     * @param {*} y Position Y
     * @param {*} w Width
     * @param {*} h Height
     */
    this.strokeRect = function(x, y, w, h) {
        updateStyles();
        context.strokeRect(x * scale, y * scale, w * scale, h * scale);
    }

    /** Draws an image.
     * @param {*} img 
     * @param {*} sx 
     * @param {*} sy 
     * @param {*} swidth 
     * @param {*} sheight 
     * @param {*} x 
     * @param {*} y 
     * @param {*} width 
     * @param {*} height 
     */
    this.drawImage = function(img, sx, sy, swidth, sheight, x, y, width, height) {
        context.drawImage(
            img, sx, sy,
            swidth, sheight,
            x * scale, y * scale, width * scale, height * scale);
    }

    /** Draws a point as a cross.
     * 
     * @param {number} x 
     * @param {number} y 
     * @param {number} size 
     * @param {string} color 
     */
    this.strokePoint = function(x, y, size, color) {
        context.beginPath();
        context.moveTo((x - size) * scale, y * scale);
        context.lineTo((x + size) * scale, y * scale);
        context.moveTo(x * scale, (y - size) * scale);
        context.lineTo(x * scale, (y + size) * scale);
        context.strokeStyle = color;
        context.stroke();
    }

    // EVENT METHODS -----------------------------------------------------------------------------//
    this.initialize = function(){
        this.resizeCanvas();
    }

    // CONSTRUCTOR -------------------------------------------------------------------------------//
    canvas = document.getElementById("game_canvas");
    context = canvas.getContext("2d");
    this.size = size;
    this.fillStyle = context.fillStyle;
    this.strokeStyle = context.strokeStyle;
    this.lineWidth = context.lineWidth;

    // INTERNAL METHODS --------------------------------------------------------------------------//
    function draw_foo() {
        log.d(that.size);
        context.fillRect(0,0, canvas.width, canvas.height);
        context.fillStyle = "#FF0000";
        context.fillRect(800 * scale, 450 * scale, 10 * scale, 10 * scale);
    }

    function updateStyles() {
        context.fillStyle = that.fillStyle;
        context.strokeStyle = that.strokeStyle;
        context.lineWidth = that.lineWidth * scale;
    }

}
