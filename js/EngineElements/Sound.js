/**
 * This class represents ...
 */

function Sound(src){
    // PUBLIC ATTRIBUTES -------------------------------------------------------------------------//
    this.sound;
    
    // PRIVATE ATTRIBUTES ------------------------------------------------------------------------//

    // PUBLIC METHODS ----------------------------------------------------------------------------//
    this.play = function(){ this.sound.play(); }

    this.stop = function(){ this.sound.pause(); }

    // EVENT METHODS -----------------------------------------------------------------------------//

    // CONSTRUCTOR -------------------------------------------------------------------------------//
    this.sound = document.createElement("audio");
    this.sound.src = src;
    this.sound.setAttribute("preload", "auto");
    this.sound.setAttribute("controls", "none");
    this.sound.style.display = "none";
    document.body.appendChild(this.sound);

    // INTERNAL METHODS --------------------------------------------------------------------------//

}