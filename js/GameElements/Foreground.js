/**
 * This class represents the foreground of the scene.
 */

function Foreground(renderer){
    // PUBLIC ATTRIBUTES -------------------------------------------------------------------------//

    // PRIVATE ATTRIBUTES ------------------------------------------------------------------------//
    var that = this;
    var floor;
    var fgRenderer;

    var frame = {
        width: 20,
        color: "white"
    };

    // PUBLIC METHODS ----------------------------------------------------------------------------//

    // EVENT METHODS -----------------------------------------------------------------------------//
    this.initialize = function() {
        floor = new Platform(new Coord(0, 800), new Coord(fgRenderer.size.x, 100));
    }

    this.update = function(deltaTime) {}
    this.render = function(renderer) {
        // Render floor
        floor.render(fgRenderer);

        // Render outer frame
        renderFrame();
    }

    // CONSTRUCTOR -------------------------------------------------------------------------------//
    fgRenderer = renderer;

    // INTERNAL METHODS --------------------------------------------------------------------------//
    function renderFrame(){
        renderer.save();

        renderer.strokeStyle = frame.color;
        renderer.lineWidth = frame.width;
        renderer.strokeRect(0, 0, renderer.size.x, renderer.size.y);

        renderer.restore();
    }
}

