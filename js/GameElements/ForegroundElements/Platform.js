/**
 * This class represents a static platform in the scene.
 */

function Platform(position, size){
    // PUBLIC ATTRIBUTES -------------------------------------------------------------------------//
    this.position;
    this.size;

    // PRIVATE ATTRIBUTES ------------------------------------------------------------------------//
    var that = this;
    var color = "#281400";

    // PUBLIC METHODS ----------------------------------------------------------------------------//

    // EVENT METHODS -----------------------------------------------------------------------------//
    this.initialize = function() {}
    
    this.update = function(deltaTime) {}

    this.render = function(renderer) {
        renderer.save();

        renderer.fillStyle = color;
        renderer.fillRect(this.position.x, this.position.y, this.size.x, this.size.y);

        renderer.restore();
    }

    // CONSTRUCTOR -------------------------------------------------------------------------------//
    this.position = position ? position : new Coord(0, 0);
    this.size = size ? size : new Coord(100, 100);

    // INTERNAL METHODS --------------------------------------------------------------------------//

}
